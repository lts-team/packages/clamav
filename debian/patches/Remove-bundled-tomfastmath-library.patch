From 4a3636dde12046a264f41384d239ee4aae5c43f9 Mon Sep 17 00:00:00 2001
From: Sebastian Andrzej Siewior <sebastian@breakpoint.cc>
Date: Sat, 18 Feb 2023 10:45:50 +0100
Subject: Remove bundled tomfastmath library.

Now that the tomfastmath library is no longer used, remove it from the
tree.

Patch-Name: Remove-bundled-tomfastmath-library.patch
Signed-off-by: Sebastian Andrzej Siewior <sebastian@breakpoint.cc>
---
 .github/workflows/clang-format.yml |   2 +-
 README.md                          |   1 -
 clamav-config.h.cmake.in           |   3 -
 libclamav/CMakeLists.txt           | 104 -----------------------------
 libclamav/Doxyfile                 |  13 +---
 unit_tests/CMakeLists.txt          |   3 -
 6 files changed, 2 insertions(+), 124 deletions(-)

diff --git a/.github/workflows/clang-format.yml b/.github/workflows/clang-format.yml
index a9d6a14..da037e2 100644
--- a/.github/workflows/clang-format.yml
+++ b/.github/workflows/clang-format.yml
@@ -22,7 +22,7 @@ name: clang-format
       matrix:
         path:
           - check: "libclamav"
-            exclude: "(iana_cctld|bytecode_api_|bytecode_hooks|rijndael|yara|inffixed|inflate|queue|tomsfastmath|nsis|7z|regex|c++|generated)"
+            exclude: "(iana_cctld|bytecode_api_|bytecode_hooks|rijndael|yara|inffixed|inflate|queue|nsis|7z|regex|c++|generated)"
           - check: "libfreshclam"
             exclude: ""
           - check: "clamav-milter"
diff --git a/README.md b/README.md
index 548d672..68dec8c 100644
--- a/README.md
+++ b/README.md
@@ -113,7 +113,6 @@ ClamAV contains a number of components that include code copied in part or in
 whole from 3rd party projects and whose code is not owned by Cisco and which
 are licensed differently than ClamAV. These include:
 
-- tomsfastmath:  public domain
 - Yara: Apache 2.0 license
   - Yara has since switched to the BSD 3-Clause License;
     Our source is out-of-date and needs to be updated.
diff --git a/clamav-config.h.cmake.in b/clamav-config.h.cmake.in
index 8153ff5..4f3b837 100644
--- a/clamav-config.h.cmake.in
+++ b/clamav-config.h.cmake.in
@@ -401,9 +401,6 @@
 /* Define if UNRAR is linked instead of loaded. */
 #cmakedefine UNRAR_LINKED 1
 
-/* Define if UNRAR is linked instead of loaded. */
-#cmakedefine HAVE_SYSTEM_TOMSFASTMATH 1
-
 /* "Full clamav library version number" */
 #define LIBCLAMAV_FULLVER "@LIBCLAMAV_VERSION@"
 
diff --git a/libclamav/CMakeLists.txt b/libclamav/CMakeLists.txt
index 5eb2e95..82f4e0a 100644
--- a/libclamav/CMakeLists.txt
+++ b/libclamav/CMakeLists.txt
@@ -23,15 +23,6 @@ endif()
 
 add_definitions(-DTHIS_IS_LIBCLAMAV)
 
-# Enable overflow checks in TomsFastMath's fp_exptmod() function.
-add_definitions(-DTFM_CHECK)
-
-# Just enable ASM in in TomsFastMath's on x86-64 where we know it works.
-# on i686 we run out of registers with -fPIC, and on ia64 we miscompile.
-if(NOT CMAKE_COMPILER_IS_GNUCC OR NOT (CMAKE_SIZEOF_VOID_P EQUAL 8))
-    add_definitions(-DTFM_NO_ASM)
-endif()
-
 # 3rd party libraries included in libclamav
 add_library( regex OBJECT )
 target_sources( regex
@@ -166,99 +157,6 @@ target_link_libraries( yara
         PCRE2::pcre2
         JSONC::jsonc )
 
-add_library( tomsfastmath OBJECT )
-target_sources( tomsfastmath
-    PRIVATE
-        tomsfastmath/addsub/fp_add.c
-        tomsfastmath/addsub/fp_add_d.c
-        tomsfastmath/addsub/fp_addmod.c
-        tomsfastmath/addsub/fp_cmp.c
-        tomsfastmath/addsub/fp_cmp_d.c
-        tomsfastmath/addsub/fp_cmp_mag.c
-        tomsfastmath/addsub/fp_sub.c
-        tomsfastmath/addsub/fp_sub_d.c
-        tomsfastmath/addsub/fp_submod.c
-        tomsfastmath/addsub/s_fp_add.c
-        tomsfastmath/addsub/s_fp_sub.c
-        tomsfastmath/bin/fp_radix_size.c
-        tomsfastmath/bin/fp_read_radix.c
-        tomsfastmath/bin/fp_read_signed_bin.c
-        tomsfastmath/bin/fp_read_unsigned_bin.c
-        tomsfastmath/bin/fp_reverse.c
-        tomsfastmath/bin/fp_s_rmap.c
-        tomsfastmath/bin/fp_signed_bin_size.c
-        tomsfastmath/bin/fp_to_signed_bin.c
-        tomsfastmath/bin/fp_to_unsigned_bin.c
-        tomsfastmath/bin/fp_toradix.c
-        tomsfastmath/bin/fp_toradix_n.c
-        tomsfastmath/bin/fp_unsigned_bin_size.c
-        tomsfastmath/bit/fp_cnt_lsb.c
-        tomsfastmath/bit/fp_count_bits.c
-        tomsfastmath/bit/fp_div_2.c
-        tomsfastmath/bit/fp_div_2d.c
-        tomsfastmath/bit/fp_lshd.c
-        tomsfastmath/bit/fp_mod_2d.c
-        tomsfastmath/bit/fp_rshd.c
-        tomsfastmath/divide/fp_div.c
-        tomsfastmath/divide/fp_div_d.c
-        tomsfastmath/divide/fp_mod.c
-        tomsfastmath/divide/fp_mod_d.c
-        tomsfastmath/exptmod/fp_2expt.c
-        tomsfastmath/exptmod/fp_exptmod.c
-        tomsfastmath/misc/fp_ident.c
-        tomsfastmath/misc/fp_set.c
-        tomsfastmath/mont/fp_montgomery_calc_normalization.c
-        tomsfastmath/mont/fp_montgomery_reduce.c
-        tomsfastmath/mont/fp_montgomery_setup.c
-        tomsfastmath/mul/fp_mul.c
-        tomsfastmath/mul/fp_mul_comba.c
-        tomsfastmath/mul/fp_mul_2.c
-        tomsfastmath/mul/fp_mul_2d.c
-        tomsfastmath/mul/fp_mul_comba_12.c
-        tomsfastmath/mul/fp_mul_comba_17.c
-        tomsfastmath/mul/fp_mul_comba_20.c
-        tomsfastmath/mul/fp_mul_comba_24.c
-        tomsfastmath/mul/fp_mul_comba_28.c
-        tomsfastmath/mul/fp_mul_comba_3.c
-        tomsfastmath/mul/fp_mul_comba_32.c
-        tomsfastmath/mul/fp_mul_comba_4.c
-        tomsfastmath/mul/fp_mul_comba_48.c
-        tomsfastmath/mul/fp_mul_comba_6.c
-        tomsfastmath/mul/fp_mul_comba_64.c
-        tomsfastmath/mul/fp_mul_comba_7.c
-        tomsfastmath/mul/fp_mul_comba_8.c
-        tomsfastmath/mul/fp_mul_comba_9.c
-        tomsfastmath/mul/fp_mul_comba_small_set.c
-        tomsfastmath/mul/fp_mul_d.c
-        tomsfastmath/mul/fp_mulmod.c
-        tomsfastmath/numtheory/fp_invmod.c
-        tomsfastmath/sqr/fp_sqr.c
-        tomsfastmath/sqr/fp_sqr_comba_12.c
-        tomsfastmath/sqr/fp_sqr_comba_17.c
-        tomsfastmath/sqr/fp_sqr_comba_20.c
-        tomsfastmath/sqr/fp_sqr_comba_24.c
-        tomsfastmath/sqr/fp_sqr_comba_28.c
-        tomsfastmath/sqr/fp_sqr_comba_3.c
-        tomsfastmath/sqr/fp_sqr_comba_32.c
-        tomsfastmath/sqr/fp_sqr_comba_4.c
-        tomsfastmath/sqr/fp_sqr_comba_48.c
-        tomsfastmath/sqr/fp_sqr_comba_6.c
-        tomsfastmath/sqr/fp_sqr_comba_64.c
-        tomsfastmath/sqr/fp_sqr_comba_7.c
-        tomsfastmath/sqr/fp_sqr_comba_8.c
-        tomsfastmath/sqr/fp_sqr_comba_9.c
-        tomsfastmath/sqr/fp_sqr_comba_generic.c
-        tomsfastmath/sqr/fp_sqr_comba_small_set.c
-        tomsfastmath/sqr/fp_sqrmod.c
-        )
-target_include_directories( tomsfastmath
-    PRIVATE
-        ${CMAKE_BINARY_DIR}
-        ${CMAKE_CURRENT_SOURCE_DIR}/tomsfastmath/headers
-    PUBLIC  ${CMAKE_CURRENT_SOURCE_DIR} )
-set_target_properties( tomsfastmath PROPERTIES
-    COMPILE_FLAGS "${WARNCFLAGS}" )
-
 # Bytecode Runtime
 add_library( bytecode_runtime OBJECT )
 if(LLVM_FOUND)
@@ -525,7 +423,6 @@ if(ENABLE_SHARED_LIB)
             regex
             lzma_sdk
             yara
-            tomsfastmath
             bytecode_runtime
             ${LIBMSPACK}
             ClamAV::libclamav_rust
@@ -637,7 +534,6 @@ if(ENABLE_STATIC_LIB)
             regex
             lzma_sdk
             yara
-            tomsfastmath
             bytecode_runtime
             ${LIBMSPACK}
             ClamAV::libclamav_rust
diff --git a/libclamav/Doxyfile b/libclamav/Doxyfile
index a83cf22..a2593ea 100644
--- a/libclamav/Doxyfile
+++ b/libclamav/Doxyfile
@@ -111,15 +111,4 @@ INPUT = . \
         jsparse \
         jsparse/generated \
         nsis \
-        regex \
-        tomsfastmath \
-        tomsfastmath/addsub \
-        tomsfastmath/bin \
-        tomsfastmath/bit \
-        tomsfastmath/divide \
-        tomsfastmath/exptmod \
-        tomsfastmath/misc \
-        tomsfastmath/mont \
-        tomsfastmath/mul \
-        tomsfastmath/numtheory \
-        tomsfastmath/sqr
+        regex
diff --git a/unit_tests/CMakeLists.txt b/unit_tests/CMakeLists.txt
index 567e95e..0b3d565 100644
--- a/unit_tests/CMakeLists.txt
+++ b/unit_tests/CMakeLists.txt
@@ -49,7 +49,6 @@ if(ENABLE_APP)
         PRIVATE
             ClamAV::libclamav
             libcheck::check
-            tomsfastmath
             JSONC::jsonc
             ${LIBMSPACK}
             OpenSSL::SSL
@@ -85,7 +84,6 @@ if(ENABLE_APP)
             ClamAV::libclamav
             ClamAV::common
             libcheck::check
-            tomsfastmath
             JSONC::jsonc
             ${LIBMSPACK}
             OpenSSL::SSL
@@ -133,7 +131,6 @@ target_link_libraries(check_clamav
     PRIVATE
         ClamAV::libclamav
         libcheck::check
-        tomsfastmath
         JSONC::jsonc
         ${LIBMSPACK}
         OpenSSL::SSL
